import Graphic, { MODE_THREEJS } from './core/Graphic';
import Simulation from './core/Simulation';
import { EPSILON, RADIUS, SPLIT } from './core/constants';
import './assets/style.scss';

/* Repository */
const REPO = 'https://gitlab.com/Yarflam/heaplifes';

/* Default constants for the simulation */
const ENERGY = 300;
const MAX_TIME = -1;

function Main(selector) {
    /* Application */
    const app = document.querySelector(selector);
    let [w, h] = [app.offsetWidth, app.offsetHeight];
    let deep = Math.max(w, h);

    /* Diagnostics */
    const info = document.createElement('div');
    info.innerHTML =
        `<a href="${REPO}" target="_blank">Source code</a> - ` +
        `Seed: [${ENERGY}, ${EPSILON}, ${RADIUS}, ${SPLIT}]`;
    info.className = 'DiagInfo';
    app.appendChild(info);

    const debug = document.createElement('div');
    debug.innerHTML = 'Starting ...';
    debug.className = 'DiagDebug';
    app.appendChild(debug);

    /* Display */
    const display = new Graphic(MODE_THREEJS).setApp(app).size(w, h, deep);
    display.init();

    /* Simulation */
    const sim = new Simulation();
    sim.genesis(ENERGY);

    /* Tools */
    const roundPos = (x, p = 10000) => Math.floor(x * p) / p;

    /* Stop the animation */
    let stopAnim = false;
    document.querySelector('body').addEventListener('keyup', e => {
        const key = e.keyCode ? e.keyCode : e.which;
        if (key == 32) {
            stopAnim = !stopAnim;
        }
    });

    /* Animate */
    let m,
        time = 0;
    const anim = () => {
        if ((time < MAX_TIME || MAX_TIME < 0) && !stopAnim) {
            /* Many actions */
            sim.action();
            m = sim.draw(display);

            /* Debug */
            time++;
            debug.innerHTML = [
                `Axis: X(${roundPos(m.x.min)},${roundPos(m.x.max)}), ` +
                    `Y(${roundPos(m.y.min)},${roundPos(m.y.max)}), ` +
                    `Z(${roundPos(m.z.min)},${roundPos(m.z.max)})`,
                'Cells: ' + m.nbCells,
                'Time: ' + time
            ].join('; ');

            /* Next frame */
            requestAnimationFrame(anim);
        } else if (display.mode() == MODE_THREEJS) {
            /* Mouse controls */
            display.render();
            requestAnimationFrame(anim);
        }
    };
    anim();
}

Main('#root');
