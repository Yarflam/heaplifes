import { EPSILON, RADIUS, SPLIT } from './constants';
import Cell from './Cell';

const POINT_SIZE = 5;
const USE_TIMELINE = false; // optimized

class Simulation {
    constructor() {
        this.universe = [];
        this.timeline = [];
    }

    genesis(e) {
        if (!this.universe.length) {
            this.universe.push(new Cell(0, 0, 0, 0, e));
        }
    }

    action() {
        const limit = this.universe.length;
        let timeline = USE_TIMELINE ? [] : null;
        /* The life is strange sometimes */
        this.measure = null;
        this.universe.map(item => {
            item.action(this.universe, limit, timeline);
            this.scaleUniverse(item);
            return item;
        });
        /* Merge the timeline if we use it */
        if (USE_TIMELINE) {
            this.timeline.push(timeline);
        }
    }

    draw(display) {
        /* Measure */
        let measure = this.adaptToDisplay(this.measure, display);

        /* Clear the screen */
        display.clear();

        /* Move the cells */
        let x, y, z, c;
        this.universe.map(item => {
            /* Calculate the projection */
            x = measure.x.unit * (item.x - measure.x.min);
            y = measure.y.unit * (item.y - measure.y.min);
            z = measure.z.unit * (item.z - measure.z.min);
            c = (1 / measure.e) * item.e;

            /* Draw */
            display.drawPoint({
                x,
                y,
                z,
                color: c,
                radius: measure.pt.size,
                item
            });

            return item;
        });

        /* Render the scene (only for 3D mode) */
        display.render();

        return measure;
    }

    /* Adapt the measures to the display */
    adaptToDisplay(measure, display) {
        /* How many cells ? */
        measure.nbCells = this.universe.length;
        /* Margin */
        (measure.x.min -= EPSILON), (measure.x.max += EPSILON);
        (measure.y.min -= EPSILON), (measure.y.max += EPSILON);
        (measure.z.min -= EPSILON), (measure.z.max += EPSILON);
        /* Vector */
        measure.x.vect = measure.x.max - measure.x.min;
        measure.y.vect = measure.y.max - measure.y.min;
        measure.z.vect = measure.z.max - measure.z.min;
        /* The ratio between the vector and the display */
        const ratio = display.ratio();
        measure.x.unit = ratio.x / measure.x.vect;
        measure.y.unit = ratio.y / measure.y.vect;
        measure.z.unit = ratio.z / measure.z.vect;
        /* The points' size */
        measure.pt = { size: POINT_SIZE };
        return measure;
    }

    /* Measure the scale of the universe */
    scaleUniverse(item) {
        if (this.measure || false) {
            /* Min */
            this.measure.x.min = Math.min(this.measure.x.min, item.x);
            this.measure.y.min = Math.min(this.measure.y.min, item.y);
            this.measure.z.min = Math.min(this.measure.z.min, item.z);
            /* Max */
            this.measure.x.max = Math.max(this.measure.x.max, item.x);
            this.measure.y.max = Math.max(this.measure.y.max, item.y);
            this.measure.z.max = Math.max(this.measure.z.max, item.z);
            this.measure.e = Math.max(this.measure.e, item.e);
        } else {
            this.measure = {
                x: { min: item.x, max: item.x },
                y: { min: item.y, max: item.y },
                z: { min: item.z, max: item.z },
                e: item.e
            };
        }
    }
}

export default Simulation;
