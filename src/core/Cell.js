import { EPSILON, RADIUS, SPLIT } from './constants';

class Cell {
    constructor(id, x, y, z, e) {
        this.id = id;
        this.x = x + Math.random() * 2 * RADIUS - RADIUS;
        this.y = y + Math.random() * 2 * RADIUS - RADIUS;
        this.z = z + Math.random() * 2 * RADIUS - RADIUS;
        this.e = e;
    }

    action(universe, limit, timeline) {
        /* Not enough energy - move and give birth is not possible */
        if (this.e < EPSILON) {
            return this;
        }

        /* Cells closest */
        const nears = universe.filter((cell, index) => {
            if (this == cell || index >= limit) {
                return false;
            }
            return this.near(cell, RADIUS);
        });

        /* Get a random cell (or decide to give birth) */
        const rand = Math.floor(Math.random() * (nears.length + 1)) - 1;

        /* Can't give birth, waiting */
        if (rand < 0 && this.e * (1 - SPLIT) < EPSILON) {
            return this;
        }

        /* Applying the decision */
        if (rand >= 0) {
            /* Move to the cell */
            const vect = this.vect(nears[rand]);
            const dist = this.dist(vect);
            if (dist > EPSILON) {
                this.x += (vect.x / dist) * EPSILON;
                this.y += (vect.y / dist) * EPSILON;
                this.z += (vect.z / dist) * EPSILON;
            }
            /* Energy exchange */
            nears[rand].e += EPSILON;
            this.e -= EPSILON;
            /* Timeline */
            if (Array.isArray(timeline)) {
                timeline.push(nears[rand].id, { e: nears[rand].e });
                timeline.push(this.id, { e: this.e });
            }
        } else {
            /* Give birth */
            const birth = new Cell(
                universe.length,
                this.x,
                this.y,
                this.z,
                this.e * SPLIT
            );
            universe.push(birth);
            /* Conservation of the energy */
            this.e *= 1 - SPLIT;
            /* Timeline */
            if (Array.isArray(timeline)) {
                timeline.push(birth.id, { e: birth.e });
                timeline.push(this.id, { e: this.e });
            }
        }
        return this;
    }

    /* Manhattan distance */
    near(obj, distance) {
        const v = obj instanceof Cell ? this.vect(obj) : obj;

        /* Filter */
        if (v.x > distance) return 0;
        if (v.y > distance) return 0;
        if (v.z > distance) return 0;

        /* Square distance */
        return this.dist(v) <= distance;
    }

    /* Distance between two cells */
    dist(obj) {
        const v = obj instanceof Cell ? this.vect(obj) : obj;
        return Math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    }

    /* Get a vector */
    vect(cell) {
        return { x: cell.x - this.x, y: cell.y - this.y, z: cell.z - this.z };
    }
}

export default Cell;
