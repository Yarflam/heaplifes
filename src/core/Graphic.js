export const MODE_SVG = 'svg';
export const MODE_CANVAS = 'canvas';
export const MODE_THREEJS = '3D';

class Graphic {
    constructor(mode = MODE_CANVAS) {
        this._mode = mode;
    }

    init() {
        /* SVG */
        if (this._mode == MODE_SVG) {
            /* SVG element */
            this.svg = require('svg.js')(this.app);
            /* Size + Resolution */
            this.svg.size(this._w || 0, this._h || 0);
            /* Elements */
            this.points = {};
        }
        /* Canvas */
        if (this._mode == MODE_CANVAS) {
            /* Canvas element */
            this.canvas = document.createElement('canvas');
            this.app.appendChild(this.canvas);
            this.ctx = this.canvas.getContext('2d');
            /* Size */
            this.canvas.style.width = this._w;
            this.canvas.style.height = this._h;
            /* Resolution */
            this.canvas.width = this._w;
            this.canvas.height = this._h;
        }
        /* 3D Three.js */
        if (this._mode == MODE_THREEJS) {
            /* Scene */
            let THREE = require('three');
            THREE.OrbitControls = require('three-orbitcontrols');
            this.THREE = THREE;
            this.scene = new THREE.Scene();
            this.camera = new THREE.PerspectiveCamera(
                45,
                this._w / this._h,
                1,
                10 * this._deep
            );
            /* Canvas element */
            this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas });
            this.renderer.setSize(this._w, this._h);
            this.app.appendChild(this.renderer.domElement);
            /* Sandbox */
            const boxCenter = [this._deep / 2, this._deep / 2, this._deep / 2];
            this.drawBox({
                x: boxCenter[0],
                y: boxCenter[1],
                z: boxCenter[2],
                size: this._deep,
                color: 0xffffff,
                opacity: 0.1
            });
            this.camera.position.set(
                boxCenter[0],
                boxCenter[1],
                5 * boxCenter[2]
            );
            /* OrbitControls */
            this.controls = new THREE.OrbitControls(
                this.camera,
                this.renderer.domElement
            );
            this.controls.target.set(...boxCenter);
            this.controls.enableDamping = true;
            this.controls.dampingFactor = 0.25;
            this.controls.enableZoom = true;
            this.controls.update();
            /* Elements */
            this.points = {};
        }
        return this;
    }

    drawPoint({ x, y, z, color, radius, item }) {
        /* SVG */
        if (this._mode == MODE_SVG) {
            if (this.points[item.id] !== undefined) {
                /* Move the point */
                this.points[item.id]
                    .attr({ fill: `rgba(255,255,255,${color})` })
                    .move(x, y);
            } else {
                /* Create the point */
                this.points[item.id] = this.svg
                    .circle(radius)
                    .attr({ fill: `rgba(255,255,255,${color})` })
                    .move(x, y);
            }
        }
        /* Canvas */
        if (this._mode == MODE_CANVAS) {
            this.drawCircle({
                x,
                y,
                radius: radius / 2,
                color: `rgba(255,255,255,${color})`,
                border: 0,
                borderColor: 'rgba(0,0,0,0)',
                mode: 1
            });
        }
        /* 3D Three.js */
        if (this._mode == MODE_THREEJS) {
            if (this.points[item.id] !== undefined) {
                /* Move the point */
                this.points[item.id].position.set(x, y, z);
                this.points[item.id].material.opacity = 0.75 * color + 0.25;
            } else {
                /* Create the point */
                this.points[item.id] = this.drawSphere({
                    x,
                    y,
                    z,
                    radius,
                    color: 0xffffff,
                    opacity: color
                });
            }
        }
    }

    render() {
        /* 3D Three.js */
        if (this._mode == MODE_THREEJS) {
            this.renderer.render(this.scene, this.camera);
        }
    }

    clear() {
        /* Canvas */
        if (this._mode == MODE_CANVAS) {
            this.drawPolygon({
                data: [[0, 0], [this._w, 0], [this._w, this._h], [0, this._h]],
                color: 'rgba(0,0,0,0.8)',
                border: 0,
                borderColor: 'rgba(0,0,0,0)',
                mode: 1
            });
        }
        return this;
    }

    /* DRAWING FUNCTIONS */

    drawPolygon(args) {
        /*
         *	@args: data (Array), color, border, borderColor, mode
         *	@example(data): [[0,0], [5,0], [5,5]]
         *	@return: obj
         */
        this.ctx.beginPath();
        this.ctx.fillStyle = args.color;
        this.ctx.lineWidth = args.border;
        this.ctx.strokeStyle = args.borderColor;
        this.ctx.moveTo(args.data[0][0], args.data[0][1]);
        for (var i = 1; i < args.data.length; i++) {
            this.ctx.lineTo(args.data[i][0], args.data[i][1]);
        }
        if (args.mode) {
            this.ctx.fill();
        } else {
            this.ctx.stroke();
        }
        this.ctx.stroke();
        this.ctx.closePath();
        return this;
    }

    drawCircle(args) {
        /*
         *	@args: x, y, radius, color, border, borderColor,
         *		shadow*, shadowColor*, startArc*, endArc*, mode
         *	@return: obj
         */
        var startArc = args.startArc !== undefined ? args.startArc : 0,
            endArc = args.endArc !== undefined ? args.endArc : 2 * Math.PI;
        this.ctx.beginPath();
        this.ctx.fillStyle = args.color;
        this.ctx.lineWidth = args.border;
        this.ctx.strokeStyle = args.borderColor;
        if (args.shadow !== undefined) {
            this.ctx.shadowBlur = args.shadow;
        }
        if (args.shadowColor !== undefined) {
            this.ctx.shadowColor = args.shadowColor;
        }
        this.ctx.arc(args.x, args.y, args.radius, startArc, endArc, false);
        if (args.mode) {
            this.ctx.fill();
        } else {
            this.ctx.stroke();
        }
        this.ctx.stroke();
        this.ctx.closePath();
        return this;
    }

    drawBox(args) {
        const THREE = this.THREE;
        /* Properties */
        const geometry = new THREE.BoxGeometry(args.size, args.size, args.size);
        const material = new THREE.MeshBasicMaterial({
            color: args.color,
            wireframe: true,
            transparent: true,
            opacity: args.opacity || 1
        });
        /* Build */
        const cube = new THREE.Mesh(geometry, material);
        cube.position.set(args.x, args.y, args.z);
        /* Assignment and return */
        this.scene.add(cube);
        return cube;
    }

    drawSphere(args) {
        const THREE = this.THREE;
        /* Properties */
        const geometry = new THREE.SphereGeometry(
            args.radius,
            args.radius,
            args.radius
        );
        const material = new THREE.MeshBasicMaterial({
            color: args.color,
            transparent: true,
            opacity: args.opacity || 1
        });
        /* Build */
        const sphere = new THREE.Mesh(geometry, material);
        sphere.position.set(args.x, args.y, args.z);
        /* Assignment and return */
        this.scene.add(sphere);
        return sphere;
    }

    /* GETTERS */

    mode() {
        return this._mode;
    }

    width() {
        return this._w;
    }

    height() {
        return this._h;
    }

    deep() {
        return this._deep;
    }

    ratio() {
        /* Specific for Three.js -> 3D sandbox */
        if (this._mode == MODE_THREEJS) {
            return {
                x: this._deep,
                y: this._deep,
                z: this._deep
            };
        }
        /* Others */
        return {
            x: this._w,
            y: this._h,
            z: 0
        };
    }

    /* SETTERS */

    size(w, h, deep = 1) {
        this._w = w;
        this._h = h;
        this._deep = deep;
        return this;
    }

    setApp(app) {
        this.app = app;
        return this;
    }
}

export default Graphic;
