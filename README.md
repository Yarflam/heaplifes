# ![logo](public/images/logo32x32.png) HeapLifes

![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)
![version](https://img.shields.io/badge/app-v1.0.0-blue.svg)

Simulation of particles in the univers.

You can try online : [https://univers-yarflam.netlify.com](https://univers-yarflam.netlify.com).

The project working now with Three.js but you can render it in 2D with canvas and svg.
You have to change the technology in ./src/index.js:
`new Graphic('3D'|'canvas'|'svg')`.

If you want, you can import the constants
MODE_SVG or MODE_CANVAS before to insert in the script.

## Prerequisites

You have to install Node.js on your computer (Windows / Linux / Mac / ARM systems).

[Download here](https://nodejs.org/en/download/)

## Install

You need to clone the project:

> $> git clone https://gitlab.com/Yarflam/heaplifes
>
> $> npm install

And you can begin to start with developer mode:

> $> npm start

Now [open this page](http://127.0.0.1:3031).

## How to work ?

The simulation is essentially based on conservation of the energy.
At the beginning, the app allocate some energy to the first cell.

Each time, the cells can move or give birth to a child. The cell can move only
to a cell in some radius around it. But a move has a cost energy and the
cell has obligation to give some part of its energy to the target cell. It's
same thing if the cell wants to give birth: it drop energy around it, the zone of
drop is defined like the new cell.

If the cell not has enough energy to move or give birth, it's can't do anything.
The cells can't die, it's a freeze effect of the spacetime ; if you didn't know
before, the temperature it's define like "particles agitations" and if no one
particle can move then we can consider the spacetime is die.

On the simulation you can see white points. Sometimes one or two of them is really
bright because the simulation has detected they have the greater level energy of
the universe. But don't worry, the master needs slaves ... it's the goal of the
"virtual cells", they have the role to share the energy between each group of cells
(heap). I didn't code the virtual cell, it's just an emergent role.

I defined three constants:

- EPSILON: it's consider like the smaller energy of the univers
(~ Planck constant). Value used: 0.1.
- RADIUS: the distance action around the cell. Value used: 0.4.
- SPLIT: the splitting cost to give birth. Value: 0.3 (30%).

Note: I have taken minimal rules and constants to define my universe because
I think it's most important when we want to build the life. I don't know if the
three dimensions of space is necessary but I want to believe the motion liberty
it's best practice to work with a complex univers. But you can imagine different
thing ... do it!

## Authors

- Yarflam - *initial work*

## License

The project is licensed under Creative Commons (BY-NC-SA) - see the [LICENSE.md](LICENSE.md) file for details.
